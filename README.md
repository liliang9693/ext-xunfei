# 讯飞语音

![](./micropython/_images/featured.png)

# 简介

掌控板micropython模式的讯飞语音，需要配合录音和wifi使用，需自行注册讯飞开放平台账号。

# 积木

![](./micropython/_images/blocks.png)

# 程序实例

### 语音识别

![](./micropython/_images/example1.png)

### 语音合成

![](./micropython/_images/example2.png)

# 讯飞语音账号获取流程
- 首先到讯飞开放平台注册并登录： https://www.xfyun.cn/
- 进入**控制台**，**创建应用**，点开创建的应用就可以得到**服务接口认证信息**也就是**APPID、APISecret、APIKey**，填入对应积木中即可。
![](./micropython/_images/xunfeizhuce.png)


# License
- MIT

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|:-----:|-----|
|mpython|||√||



# 更新日志

V0.0.1 基础功能完成

